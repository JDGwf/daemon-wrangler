use std::net::TcpStream;
use ssh2::Session;
use std::path::Path;

// use core::result::Result::Err;
const TIMEOUT: u32  = 500; // ms
pub async fn ssh_connect(
    _label: &str,
    host: &str,
    username: &str,
    ssh_private_key_path: &str,
    port: u32,
) -> Result<Session, String>  {

    let tcp_result: Result<TcpStream, std::io::Error> = TcpStream::connect(host.to_owned() + ":" + &port.to_string());

    match tcp_result {
        Ok( tcp ) => {
            let mut session: Session = Session::new().unwrap();
            session.set_timeout(TIMEOUT);
            session.set_tcp_stream(tcp);
            match session.handshake() {
                Ok(_) => {

                    let private_key = Path::new( ssh_private_key_path );
                    match session.userauth_pubkey_file(
                        username,
                        None,
                        private_key,
                        None,
                    ) {
                        Ok(_) => {

                            return Ok(session);
                        }

                        Err( err ) => {
                            // return Err(fmt::Error); //"Cannot connect to the host via TCP");
                            return Err(  format!("Cannot connect via SSH to {}: {:?}", host, err).to_string());
                        }
                    }

                    // return Ok(session);
                }

                Err( err ) => {
                    // return Err(fmt::Error); //"Cannot connect to the host via TCP");
                    return Err(  format!("Cannot connect create session handshake to {}: {:?}", host, err).to_string());
                }
            }

        }
        Err( err ) => {
            // return Err(fmt::Error); //"Cannot connect to the host via TCP");
            return Err(  format!("Cannot connect via TCP to {}: {:?}", host, err).to_string());
        }
    }

}
