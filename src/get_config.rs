use std::{fs, process::exit};
use std::path::Path;
use crate::get_host::SessionAndHost;
use crate::ssh_connect::ssh_connect;
use ssh2::Session;
use yaml_rust2::{YamlLoader, Yaml};


pub async fn get_config() -> Vec<SessionAndHost> {
    const CONFIG_FILE_PATH: &str = "./config.yaml";

    let mut sessions: Vec<SessionAndHost> = Vec::new();

    if !Path::new( CONFIG_FILE_PATH ).exists() {
        println!("{} does not exist!", CONFIG_FILE_PATH);
        exit(1);
    }
    let config_contents = fs::read_to_string( CONFIG_FILE_PATH )
        .expect("Should have been able to read the file");
    // println!("contents: {}", config_contents);
    let docs_result = YamlLoader::load_from_str( &config_contents );
    match docs_result {
        Ok(docs) =>  {
            let doc: &_ = &docs[0];
            // println!("doc {:?}", doc);
            // println!("doc {:?}", doc["hosts"]);

            let mut global_key_file = "";
            if doc["key_file"] != Yaml::Null && doc["key_file"] != Yaml::BadValue {
                global_key_file = doc["key_file"].as_str().unwrap();
            }
            if doc["hosts"] != Yaml::Null && doc["hosts"] != Yaml::BadValue {
                // let hosts_list: Hash = doc["hosts"].into_hash();
                let hosts_hash = doc["hosts"].as_hash().unwrap();
                for yaml_host in hosts_hash.into_iter() {
                    // println!("host 0 {:?}", host.0);
                    // println!("host 1 {:?}", host.1);

                    let name = yaml_host.0.as_str().unwrap();
                    let mut host = yaml_host.0.as_str().unwrap();
                    let mut user = "";
                    let private_key_default = global_key_file.to_string();
                    let mut private_key = private_key_default.as_str();
                    let mut port = 22;

                    let host_values_hash = yaml_host.1.as_hash().unwrap();
                    for item in host_values_hash {
                        // println!("item key{:?}", item.0);
                        // println!("item val{:?}", item.1);

                        if item.0.ne( &Yaml::Null ) && item.1.ne( &Yaml::Null ) {
                            match item.0.as_str().unwrap() {
                                "user" => {
                                    user = item.1.as_str().unwrap();
                                }
                                "host" => {
                                    host = item.1.as_str().unwrap();
                                }
                                "key_file" => {
                                    private_key = item.1.as_str().unwrap();
                                }
                                "port" => {
                                    port = item.1.as_i64().unwrap();
                                }
                                _ => {

                                }
                            }
                        }
                    }

                    let session_result: Result<Session, String> = ssh_connect(
                        name,
                        host,
                        user,
                        private_key,
                        port as u32,
                    ).await;

                    sessions.push( SessionAndHost{
                        session: session_result,
                        name: name.to_string(),
                        host: host.to_string(),
                        username: user.to_string(),
                        ssh_private_key_path: private_key.to_string(),
                        port: port as u32,
                    } );

                }
            }
        }
        Err(err) => {
            println!("yaml error: {}", err);
        }
    }

    return sessions;
}