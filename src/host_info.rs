use crate::filesystem_item::FilesystemItem;
use crate::process_item::ProcessItem;

#[derive(Debug)]
pub struct HostInfo {
    pub config_name: String,
    pub config_host: String,
    pub hostname: String,
    pub platform: String,
    pub kernel: String,
    pub processes: Vec<ProcessItem>,
    pub filesystem: Vec<FilesystemItem>,
}

impl HostInfo {
    pub fn print(&self) {
        // println!( "{:?}", self.processes);
        // println!( "{:?}", self.filesystem);
        println!("---------------------------");
        println!("Name: {}", self.config_name);
        println!("Host: {}", self.config_host);
        println!("Hostname: {}", self.hostname);
        println!("Platform: {}", self.platform);
        println!("Kernel: {}", self.kernel);
        println!("# Processes: {}", self.processes.len());
        println!("# File Systems: {}", self.filesystem.len());
    }
}
