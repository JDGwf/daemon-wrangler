use ssh2::Session;
use crate::process_item::ProcessItem;
use crate::filesystem_item::FilesystemItem;
use crate::host_info::HostInfo;
use crate::fetch_command::fetch_command;

pub struct SessionAndHost {
    pub session: Result<Session, String>,
    pub name: String,
    pub host: String,
    pub username: String,
    pub ssh_private_key_path: String,
    pub port: u32,
}

pub async fn get_host(
    session: &Session,
    config_host: String,
    config_name: String,
) -> HostInfo  {

    let mut rv: HostInfo = HostInfo {
        config_name: config_name.to_owned(),
        platform: "".to_owned(),
        config_host: config_host.to_owned(),
        hostname: "".to_owned(),
        kernel: "".to_owned(),
        processes: Vec::new(),
        filesystem: Vec::new(),
    };

    let uname = fetch_command( &session, "uname" ).await;
    if !uname.starts_with("COMMAND READ ERR") {
        rv.platform = uname.to_owned();
    }

    let uname_aux = fetch_command( &session, "uname -a" ).await;
    if !uname_aux.starts_with("COMMAND READ ERR") {
        rv.kernel = uname_aux.trim().to_owned();
        let split: Vec<&str> = uname_aux.split_whitespace().collect();
        if split.len() > 3  {
            rv.hostname = split[1].to_owned();
            rv.kernel = split[2].to_owned();
        }
    }

    let process_list = fetch_command( &session, "ps -aux" ).await;

    for line in process_list.split('\n')  {

        if !line.starts_with("COMMAND READ ERR") {
            let split: Vec<&str> = line.split_whitespace().collect();

            if split.len() > 9 && split[0].to_string().ne("USER")  {
                let mut process_item: ProcessItem = ProcessItem {
                    user: split[0].to_string(),
                    id: split[1].parse().unwrap(),
                    percent_cpu: split[2].parse().unwrap(),
                    percent_memory: split[3].parse().unwrap(),
                    vsz: split[4].parse().unwrap(),
                    rss: split[5].parse().unwrap(),
                    tt: split[6].to_string(),
                    stat: split[7].to_string(),
                    started: split[8].to_string(),
                    time: split[9].to_string(),
                    command: split[10].to_string(),
                };

                // append any extra command line whitespace items
                for count in 11..split.len() {
                    process_item.command += " ";
                    process_item.command += split[count].as_ref();
                }

                rv.processes.push( process_item );
                // println!("{:?}", process_item);
            }
        }
    }

    let disk_list = fetch_command( &session, "df" ).await;

    for line in disk_list.split('\n')  {

        if !line.starts_with("COMMAND READ ERR") {
            let split: Vec<&str> = line.split_whitespace().collect();

            if split.len() > 5 && split[0].to_string().ne("Filesystem")  {
                let mut filesystem_item: FilesystemItem = FilesystemItem {
                    filesystem: split[0].parse().unwrap(),
                    size:  split[1].parse().unwrap(),
                    used:  split[2].parse().unwrap(),
                    available:  split[3].parse().unwrap(),
                    capacity:  split[4].to_string().replace("%", "").parse().unwrap(),
                    mount: split[5].to_string(),
                };

                // append any extra command line whitespace items
                for count in 6..split.len() {
                    filesystem_item.mount += " ";
                    filesystem_item.mount += split[count].as_ref();
                }

                // println!("{:?}", filesystem_item);
                rv.filesystem.push( filesystem_item );
            }
        }
    }

    return rv;
}
