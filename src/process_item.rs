
#[derive(Debug)]
pub struct ProcessItem {
    pub user: String,
    pub id: u32,
    pub percent_cpu: f32, // %CPU
    pub percent_memory: f32, // %MEM
    pub vsz: u32, // VSZ
    pub rss: u32, // RSS
    pub tt: String, // TT
    pub stat: String, // STAT
    pub started: String, // STARTED
    pub time: String, // TIME
    pub command: String, // COMMAND
}