
#[derive(Debug)]
pub struct FilesystemItem {
    pub filesystem: String,
    pub size: u64,
    pub used: u64,
    pub available: u64,
    pub capacity: f32,
    pub mount: String,
}