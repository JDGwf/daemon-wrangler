
mod process_item;
mod fetch_command;
mod host_info;
mod filesystem_item;
mod get_config;
mod get_host;
mod ssh_connect;
use std::thread::sleep;
use std::time::Duration;
use get_config::get_config;
use get_host::get_host;
use ssh_connect::ssh_connect;

#[tokio::main]
async fn main() {

    let mut sessions = get_config().await;
    loop {
        for session in sessions.iter_mut() {
            println!("---------------------------");
            println!("| Trying {}", session.host);

            match &session.session {
                Ok( session_handler ) => {
                    println!("| Connected! ");
                    let host = get_host(
                        &session_handler,
                        session.host.to_owned(),
                        session.name.to_owned(),
                    ).await;
                    host.print();
                }
                Err( _err ) => {
                    // try to reconnect
                    println!("| Reconnecting to {}", &session.name);

                    session.session = ssh_connect(
                        &session.name,
                        &session.host,
                        &session.username,
                        &session.ssh_private_key_path,
                        session.port,
                    ).await;
                }
            }


            sleep(Duration::new(1, 0));
        }

    }
}

