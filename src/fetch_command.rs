use std::io::prelude::*;
use ssh2::Session;

pub async fn fetch_command(
    session: &Session,
    command: &str,
) -> String  {

    let mut rv: String = String::new();

    match session.channel_session() {
        Ok(mut channel) => {
            match channel.exec( command ) {
                Ok(_) => {

                    match channel.read_to_string(&mut rv) {
                        Ok(_) => {

                        }
                        Err( _err ) => {
                            rv = "COMMAND READ ERR: read_to_string()".to_string();
                        }
                    }
                    let _ = channel.wait_close();
                }
                Err( _ ) => {
                    rv = "COMMAND READ ERR: exec()".to_string();
                }
            }

        }
        Err( _err ) => {
            rv = "COMMAND READ ERR: channel_session()".to_string();
        }
    }


    return rv.trim().to_owned();
}
