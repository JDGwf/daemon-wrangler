# Daemon Wrangler

## What is it?

Right now, a thought experiment to connect to various servers via ssh and get a running status of the servers.

# Requirements

SSH keys for connecting to servers. No passwords allowed - be safe, be secure. Connect to your servers via ssh keys beforehand to get the known_hosts set up in your ssh directory

## Is it usable for me?

Nope. Maybe in a few months.

## What does it work on?

Right now connecting to both Linux and FreeBSD servers seems to work.

## What are the UI plans for this?

Ideally, I'm planning for a FreeBSD Cockpit to manage bhyve and jails as well as monitor systems.

I'm also thinking about a daemonizable CLI app which can notify if certain parameters are met.

**It is my full intent that each of the client servers do not require anything except ssh access, the wranger will parse all the data as needed**